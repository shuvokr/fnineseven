//
//  NineSeven0.swift
//  FNineSeven
//
//  Created by Shuvo on 4/9/21.
//  https://betterprogramming.pub/ios-build-your-cocoapods-framework-with-an-example-app-from-scratch-fd0f7bdf3f8c

import Foundation
import UIKit

public class NineSeven00 {
    let hello = "Hello"

    public init() {}

    public func hello(to whom: String) -> String {
        return "Hello \(whom)"
    }
    public func animateuh(view: UIView, centerview: UIView) {
        centerview.isHidden = false
        view.customAnimtationToUnnhide()
    }
    public func animateh(view: UIView, centerview: UIView) {
        centerview.isHidden = true
        view.customAnimtationToHide()
    }
    public func isDigit(phrase : String) -> Bool {
        let num = Int(phrase)
        if num != nil {
            return true
        }
        return false
    }
    public func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    public func addLoder(view : UIView) {
        view.activityStartAnimating(activityColor: .white, backgroundColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.6))
    }
    public func removeLoder(view : UIView) {
        
    }
}
extension UIView {
    func customAnimtationToUnnhide() {
        UIView.animate(withDuration: 2.0, delay: 0.0, options: .curveEaseOut, animations: {
//            self.transform = CGAffineTransform(scaleX: 100.0, y: 100.0)
            self.isHidden = false
        }, completion: nil)
    }
    func customAnimtationToHide() {
        UIView.animate(withDuration: 2.0, delay: 0.0, options: .curveEaseIn, animations: {
//            self.transform = CGAffineTransform(scaleX: 100.0, y: 100.0)
            self.isHidden = true
        }, completion: nil)
    }
    func activityStartAnimating(activityColor: UIColor, backgroundColor: UIColor) {
        let backgroundView = UIView()
        backgroundView.frame = CGRect.init(x: 0, y: 0, width: self.bounds.width, height: self.bounds.height)
        backgroundView.backgroundColor = backgroundColor
        backgroundView.tag = 475647
        
        var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
        activityIndicator = UIActivityIndicatorView(frame: CGRect.init(x: 0, y: 0, width: 50, height: 50))
        activityIndicator.center = self.center
        activityIndicator.hidesWhenStopped = true
        if #available(iOS 13.0, *) {
            activityIndicator.style = UIActivityIndicatorView.Style.medium
        } else {
            // Fallback on earlier versions
            activityIndicator.style = UIActivityIndicatorView.Style.white
        }
        activityIndicator.color = activityColor
        activityIndicator.startAnimating()
        self.isUserInteractionEnabled = false
        
        backgroundView.addSubview(activityIndicator)

        self.addSubview(backgroundView)
    }

    func activityStopAnimating() {
        if let background = viewWithTag(475647){
            background.removeFromSuperview()
        }
        self.isUserInteractionEnabled = true
    }
}
