//
//  FNineSevenTests.swift
//  FNineSevenTests
//
//  Created by Shuvo on 4/9/21.
//

import XCTest
@testable import FNineSeven

class FNineSevenTests: XCTestCase {

    func testHelloWorld() {
        let hw = NineSeven00()

        // test public method
        XCTAssertEqual(hw.hello(to: "World"), "Hello World")
    }

}
