#
#  Be sure to run `pod spec lint FNineSeven.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://guides.cocoapods.org/syntax/podspec.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|

  spec.name          = "FNineSeven"
  spec.version       = "0.2.1"
  spec.summary       = "iOS SDK for Hello World, including example app"
  spec.homepage      = "https://github.com/shuvokr"
  spec.license       = { :type => "MIT", :file => "LICENSE" }
  spec.author        = { "looktou" => "shuvosksk@gmail.com" }
  spec.platform      = :ios, "11.0"
  spec.swift_version = "5.0"
  spec.source        = {
    :git => "https://shuvokr@bitbucket.org/shuvokr/fnineseven.git",
    :tag => "#{spec.version}"
  }
  spec.source_files        = "FNineSeven/**/*.{h,m,swift}"
  spec.public_header_files = "FNineSeven/**/*.h"

end
